package mx.tgc.model.util;

import java.io.Serializable;

public class CustomSelectItem implements Serializable {
    /**
     * Variables de clase
     */
    private String label;
    private int serie;
    private long identificador;
    
    /**
     * M�todos
     */
    public CustomSelectItem() {
        super();
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setSerie(int serie) {
        this.serie = serie;
    }

    public int getSerie() {
        return serie;
    }

    public void setIdentificador(long identificador) {
        this.identificador = identificador;
    }

    public long getIdentificador() {
        return identificador;
    }
}
