package mx.tgc.model.util;

import java.util.ListResourceBundle;

public class CustomMessageBundle extends ListResourceBundle {
    private static final Object[][] sMessageStrings = new String[][] 
        {
       /*Unique constraint error */
       { "PR_TMPR_UK","La clave ya existe, agregue una clave diferente."},
       /*Primary key database */
       /* JBO Unique key too many objects for Primary key */
       //{ "25013", " La clave que trata de ingresar ya existe, por favor ingrese una clave distinta."},
       {"PR_PRPC_PR_PRBA_FK", "El bloque que está intentando eliminar tiene características asignadas. Si realmente desea eliminar el bloque, primero desasigne las características del bloque."}

};

    /**Return String Identifiers and corresponding Messages in a two-dimensional array.
     */
    protected Object[][] getContents() {
        return sMessageStrings;
    }
}
