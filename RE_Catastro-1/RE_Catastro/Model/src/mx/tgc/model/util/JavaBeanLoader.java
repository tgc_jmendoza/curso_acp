package mx.tgc.model.util;

import java.sql.Date;

import mx.net.tgc.recaudador.resource.pc.DomicilioNotificar;
import mx.net.tgc.recaudador.resource.pc.SujetoObjeto;
import mx.net.tgc.recaudador.resource.pr.CaracteristicaAsentamiento;
import mx.net.tgc.recaudador.resource.pr.PredioBloqueAgrupacion;
import mx.net.tgc.recaudador.resource.pr.PresentacionCaracteristica;
import mx.net.tgc.recaudador.resource.pr.CaracteristicaPredio;
import mx.net.tgc.recaudador.resource.pr.PredialCaracteristica;
import mx.net.tgc.recaudador.resource.pr.Predio;
import mx.net.tgc.recaudador.resource.pr.CaracteristicaPredioComponente;

import mx.net.tgc.recaudador.resource.pr.DomicilioGeografico;

import mx.net.tgc.recaudador.resource.pr.SegmentoCatastral;

import mx.tgc.model.ro.catastro.PrSegmentoCatastralVVORowImpl;
import mx.tgc.model.ro.consulta.PcDomicilioNotificarVVORowImpl;
import mx.tgc.model.ro.consulta.PcDomiciliosGeograficosVVORowImpl;
import mx.tgc.model.ro.consulta.PcSujetosObjetosVVORowImpl;
import mx.tgc.model.ro.consulta.PrCaractPredComponentesVVORowImpl;
import mx.tgc.model.ro.consulta.PrDatosPredioVVORowImpl;
import mx.tgc.model.view.catastro.PrCaracteristicasPrediosVORowImpl;
import mx.tgc.model.view.catastro.PrPrediosVORowImpl;
import mx.tgc.model.view.movimiento.PrPredialCaracteristicasVORowImpl;
import mx.tgc.model.view.movimiento.PrPresentacionCaracteristicaVORowImpl;
import mx.tgc.model.view.movimiento.PrPredBloquesAgrupacionesVORowImpl;
import mx.tgc.model.view.ubicacion.PrCaractAsentamientosVORowImpl;

public class JavaBeanLoader {

    public static CaracteristicaPredio cargaCaracteristicasPredios(PrCaracteristicasPrediosVORowImpl row) {
        CaracteristicaPredio caracteristicaPredio = new CaracteristicaPredio();
        caracteristicaPredio.setPrPredSerie(row.getPrPredSerie().intValue());
        caracteristicaPredio.setPrPredIdentificador(row.getPrPredIdentificador().longValue());
       // FIXME Enlazar vistas de detalles y caracteristicas para que al obtener row de detalle traiga su row de caracteristica
        // y cargarlo aqui
        caracteristicaPredio.setEstatus(row.getEstatus());
        caracteristicaPredio.setValor1(row.getValor1());
        caracteristicaPredio.setPrMopeSerie(row.getPrMopeSerie().intValue());
        caracteristicaPredio.setPrMopeIdentificador(row.getPrMopeIdentificador().longValue());

        if (row.getValor2() != null) {
            caracteristicaPredio.setValor2(row.getValor2());
        }
        if (row.getValor3() != null) {
            caracteristicaPredio.setValor3(row.getValor3());
        }
        if (row.getValor4() != null) {
            caracteristicaPredio.setValor4(row.getValor4());
        }
        if (row.getCampo1() != null) {
            caracteristicaPredio.setCampo1(row.getCampo1());
        }
        if (row.getCampo2() != null) {
            caracteristicaPredio.setCampo2(row.getCampo2());
        }
        if (row.getCampo3() != null) {
            caracteristicaPredio.setCampo3(row.getCampo3());
        }
        if (row.getCampo4() != null) {
            caracteristicaPredio.setCampo4(row.getCampo4());
        }
        if (row.getCampo5() != null) {
            caracteristicaPredio.setCampo5(row.getCampo5());
        }
        if (row.getCampo6() != null) {
            caracteristicaPredio.setCampo6(row.getCampo6());
        }
        if (row.getCampo7() != null) {
            caracteristicaPredio.setCampo7(row.getCampo7());
        }
        if (row.getCampo8() != null) {
            caracteristicaPredio.setCampo8(row.getCampo8());
        }
        if (row.getCampo9() != null) {
            caracteristicaPredio.setCampo9(row.getCampo9());
        }
        if (row.getCampo10() != null) {
            caracteristicaPredio.setCampo10(row.getCampo10());
        }
        if (row.getCampo11() != null) {
            caracteristicaPredio.setCampo11(row.getCampo11());
        }
        if (row.getCampo12() != null) {
            caracteristicaPredio.setCampo12(row.getCampo12());
        }
        if (row.getCampo13() != null) {
            caracteristicaPredio.setCampo13(row.getCampo13());
        }
        if (row.getCampo14() != null) {
            caracteristicaPredio.setCampo14(row.getCampo14());
        }
        if (row.getCampo15() != null) {
            caracteristicaPredio.setCampo15(row.getCampo15());
        }

        caracteristicaPredio.setCreadoPor(row.getCreadoPor());
        caracteristicaPredio.setCreadoEl(new Date(row.getCreadoEl().getValue().getTime()));
        caracteristicaPredio.setModificadoPor(row.getModificadoPor());
        caracteristicaPredio.setModificadoEl(new Date(row.getModificadoEl().getValue().getTime()));

        return caracteristicaPredio;
    }

    public static PredialCaracteristica cargaPredialCaracteristicas(PrPredialCaracteristicasVORowImpl row) {
        PredialCaracteristica predialCaracteristicas =
            new PredialCaracteristica();
        predialCaracteristicas.setSerie(row.getSerie().intValue());
        predialCaracteristicas.setIdentificador(row.getIdentificador().longValue());
        predialCaracteristicas.setNombre(row.getNombre());
        predialCaracteristicas.setEtiqueta(row.getEtiqueta());
        predialCaracteristicas.setTipoDeDato(row.getTipoDeDato());
        predialCaracteristicas.setLongitudMaxima(row.getLongitudMaxima().longValue());
        predialCaracteristicas.setLongitudDespliegue(row.getLongitudDespliegue().longValue());
        predialCaracteristicas.setTipoDeValidacion(row.getTipoDeValidacion());
        predialCaracteristicas.setRequerida(row.getRequerida());
        predialCaracteristicas.setEstatus(row.getEstatus());
        predialCaracteristicas.setOrden(row.getOrden().intValue());
        predialCaracteristicas.setComportamiento(row.getComportamiento());
        predialCaracteristicas.setNivel(row.getNivel());
        predialCaracteristicas.setPrTiavSerie(row.getPrTiavSerie().intValue());
        predialCaracteristicas.setPrTiavIdentificador(row.getPrTiavIdentificador().longValue());

        if (row.getCampo1() != null) {
            predialCaracteristicas.setCampo1(row.getCampo1());
        }
        if (row.getCampo2() != null) {
            predialCaracteristicas.setCampo2(row.getCampo2());
        }
        if (row.getCampo3() != null) {
            predialCaracteristicas.setCampo3(row.getCampo3());
        }
        if (row.getCampo4() != null) {
            predialCaracteristicas.setCampo4(row.getCampo4());
        }
        if (row.getCampo5() != null) {
            predialCaracteristicas.setCampo5(row.getCampo5());
        }
        if (row.getCampo6() != null) {
            predialCaracteristicas.setCampo6(row.getCampo6());
        }
        if (row.getCampo7() != null) {
            predialCaracteristicas.setCampo7(row.getCampo7());
        }
        if (row.getCampo8() != null) {
            predialCaracteristicas.setCampo8(row.getCampo8());
        }
        if (row.getCampo9() != null) {
            predialCaracteristicas.setCampo9(row.getCampo9());
        }
        if (row.getCampo10() != null) {
            predialCaracteristicas.setCampo10(row.getCampo10());
        }
        if (row.getCampo11() != null) {
            predialCaracteristicas.setCampo11(row.getCampo11());
        }
        if (row.getCampo12() != null) {
            predialCaracteristicas.setCampo12(row.getCampo12());
        }
        if (row.getCampo13() != null) {
            predialCaracteristicas.setCampo13(row.getCampo13());
        }
        if (row.getCampo14() != null) {
            predialCaracteristicas.setCampo14(row.getCampo14());
        }
        if (row.getCampo15() != null) {
            predialCaracteristicas.setCampo15(row.getCampo15());
        }
        predialCaracteristicas.setCreadoPor(row.getCreadoPor());
        predialCaracteristicas.setCreadoEl(new Date(row.getCreadoEl().getValue().getTime()));
        predialCaracteristicas.setModificadoPor(row.getModificadoPor());
        predialCaracteristicas.setModificadoEl(new Date(row.getCreadoEl().getValue().getTime()));

        return predialCaracteristicas;
    }

    public static PresentacionCaracteristica cargaPresentacionCaracteristica(PrPresentacionCaracteristicaVORowImpl row) {
        PresentacionCaracteristica presentacionCaracteristica =
            new PresentacionCaracteristica();

        presentacionCaracteristica.setPrPrcaSerie(row.getPrPrcaSerie().intValue());
        presentacionCaracteristica.setPrPrcaIdentificador(row.getPrPrcaIdentificador().longValue());
        presentacionCaracteristica.setPrPrbaIdentificador(row.getPrPrbaIdentificador().longValue());
        presentacionCaracteristica.setOrden(row.getOrden().intValue());
        presentacionCaracteristica.setCapturable(row.getCapturable());

        if (row.getCampo1() != null) {
            presentacionCaracteristica.setCampo1(row.getCampo1());
        }
        if (row.getCampo2() != null) {
            presentacionCaracteristica.setCampo2(row.getCampo2());
        }
        if (row.getCampo3() != null) {
            presentacionCaracteristica.setCampo3(row.getCampo3());
        }
        if (row.getCampo4() != null) {
            presentacionCaracteristica.setCampo4(row.getCampo4());
        }
        if (row.getCampo5() != null) {
            presentacionCaracteristica.setCampo5(row.getCampo5());
        }
        if (row.getCampo6() != null) {
            presentacionCaracteristica.setCampo6(row.getCampo6());
        }
        if (row.getCampo7() != null) {
            presentacionCaracteristica.setCampo7(row.getCampo7());
        }
        if (row.getCampo8() != null) {
            presentacionCaracteristica.setCampo8(row.getCampo8());
        }
        if (row.getCampo9() != null) {
            presentacionCaracteristica.setCampo9(row.getCampo9());
        }
        if (row.getCampo10() != null) {
            presentacionCaracteristica.setCampo10(row.getCampo10());
        }
        if (row.getCampo11() != null) {
            presentacionCaracteristica.setCampo11(row.getCampo11());
        }
        if (row.getCampo12() != null) {
            presentacionCaracteristica.setCampo12(row.getCampo12());
        }
        if (row.getCampo13() != null) {
            presentacionCaracteristica.setCampo13(row.getCampo13());
        }
        if (row.getCampo14() != null) {
            presentacionCaracteristica.setCampo14(row.getCampo14());
        }
        if (row.getCampo15() != null) {
            presentacionCaracteristica.setCampo15(row.getCampo15());
        }

        presentacionCaracteristica.setCreadoPor(row.getCreadoPor());
        presentacionCaracteristica.setCreadoEl(new Date(row.getCreadoEl().getValue().getTime()));
        presentacionCaracteristica.setModificadoPor(row.getModificadoPor());
        presentacionCaracteristica.setModificadoEl(new Date(row.getModificadoEl().getValue().getTime()));
        return presentacionCaracteristica;
    }

    public static PredioBloqueAgrupacion cargaBloquesAgrupaciones(PrPredBloquesAgrupacionesVORowImpl row) {
        PredioBloqueAgrupacion bloqueAgrupacion = new PredioBloqueAgrupacion();
        bloqueAgrupacion.setIdentificador(row.getIdentificador().longValue());
        bloqueAgrupacion.setNombre(row.getNombre());
        bloqueAgrupacion.setEtiqueta(row.getEtiqueta());
        bloqueAgrupacion.setOrden(row.getOrden().intValue());
        // FIXME cambiar por nivel bloqueAgrupacion.setPantalla(row.getPantalla().intValue());
        if (row.getPrPrbaIdentificador() != null) {
            bloqueAgrupacion.setPrPrbaIdentificador(row.getPrPrbaIdentificador().longValue());
        }
        if (row.getCampo1() != null) {
            bloqueAgrupacion.setCampo1(row.getCampo1());
        }
        if (row.getCampo2() != null) {
            bloqueAgrupacion.setCampo2(row.getCampo2());
        }
        if (row.getCampo3() != null) {
            bloqueAgrupacion.setCampo3(row.getCampo3());
        }
        if (row.getCampo4() != null) {
            bloqueAgrupacion.setCampo4(row.getCampo4());
        }
        if (row.getCampo5() != null) {
            bloqueAgrupacion.setCampo5(row.getCampo5());
        }
        if (row.getCampo6() != null) {
            bloqueAgrupacion.setCampo6(row.getCampo6());
        }
        if (row.getCampo7() != null) {
            bloqueAgrupacion.setCampo7(row.getCampo7());
        }
        if (row.getCampo8() != null) {
            bloqueAgrupacion.setCampo8(row.getCampo8());
        }
        if (row.getCampo9() != null) {
            bloqueAgrupacion.setCampo9(row.getCampo9());
        }
        if (row.getCampo10() != null) {
            bloqueAgrupacion.setCampo10(row.getCampo10());
        }
        if (row.getCampo11() != null) {
            bloqueAgrupacion.setCampo11(row.getCampo11());
        }
        if (row.getCampo12() != null) {
            bloqueAgrupacion.setCampo12(row.getCampo12());
        }
        if (row.getCampo13() != null) {
            bloqueAgrupacion.setCampo13(row.getCampo13());
        }
        if (row.getCampo14() != null) {
            bloqueAgrupacion.setCampo14(row.getCampo14());
        }
        if (row.getCampo15() != null) {
            bloqueAgrupacion.setCampo15(row.getCampo15());
        }
        bloqueAgrupacion.setCreadoPor(row.getCreadoPor());
        bloqueAgrupacion.setCreadoEl(new Date(row.getCreadoEl().getValue().getTime()));
        bloqueAgrupacion.setModificadoPor(row.getModificadoPor());
        bloqueAgrupacion.setModificadoEl(new Date(row.getCreadoEl().getValue().getTime()));

        return bloqueAgrupacion;
    }

    public static Predio cargaPredios(PrPrediosVORowImpl row) {
        Predio predio = new Predio();

        predio.setObjeSerie(row.getPcObjeSerie().intValue());
        predio.setObjeIdentificador(row.getPcObjeIdentificador().longValue());
        predio.setClaveCatastral(row.getClaveCatastral());
        predio.setNombrePredio(row.getNombrePredio());
        predio.setDescripcionTerreno(row.getDescripcionTerreno());
        predio.setAnioAlta(row.getAnioAlta().intValue());
        predio.setPeriodoAlta(row.getPeriodoAlta().intValue());
        predio.setAnioVigencia(row.getAnioVigencia().intValue());
        predio.setPeriodoVigencia(row.getPeriodoVigencia().intValue());
        predio.setPredioEnCondominio(row.getPredioEnCondominio());
        predio.setIndiviso(row.getIndiviso().doubleValue());
        predio.setCondominioMaestro(row.getCondominioMaestro());
        predio.setSuperficieTotalTerreno(row.getSuperficieTotalTerreno().doubleValue());
        predio.setSuperficieTerrenoPropia(row.getSuperficieTerrenoPropia().doubleValue());
        predio.setSuperficieTerrenoComun(row.getSuperficieTerrenoComun().doubleValue());
        predio.setSuperficieTerrenoExtra(row.getSuperficieTerrenoExtra().doubleValue());
        predio.setSuperficieTotalConstruccion(row.getSuperficieTotalConstruccion().doubleValue());
        predio.setSuperficieConstruccionPropia(row.getSuperficieConstruccionPropia().doubleValue());
        predio.setSuperficieConstruccionComun(row.getSuperficieConstruccionComun().doubleValue());
        predio.setSuperficieConstruccionExtra(row.getSuperficieConstruccionExtra().doubleValue());
        predio.setTipoPredio(row.getTipoPredio());
        predio.setTipoPropiedad(row.getTipoPropiedad());
        predio.setAcumuladoPeriodos(row.getAcumuladoPeriodos().intValue());
        predio.setAcumuladoImpuesto(row.getAcumuladoImpuesto().doubleValue());
        predio.setProgramaActual(row.getProgramaActual());
        predio.setEstatus(row.getEstatus());
        predio.setFolioReal(row.getFolioReal());
        predio.setFechaInscripcionFolio(row.getFechaInscripcionFolio().dateValue());
        predio.setCurt(row.getCurt());
        predio.setNivel(row.getNivel());
        predio.setCentroideLatitud(row.getCentroideLatitud());
        predio.setCentroideLongitud(row.getCentroideLongitud());
        predio.setPeriodicidadAdeudo(row.getPeriodicidadAdeudo().intValue());
        predio.setSecaSerie(row.getPrSecaSerie().intValue());
        predio.setSecaIdentificador(row.getPrSecaIdentificador().longValue());
        predio.setTipoAvaluoSerie(row.getPrTiavSerie().intValue());
        predio.setTipoAvaluoIdentificador(row.getPrTiavIdentificador().longValue());
        predio.setGiroSerie(row.getPrGiroSerie().intValue());
        predio.setGiroIdentificador(row.getPrGiroIdentificador().longValue());
        predio.setPredObjeSerie(row.getPrPredPcObjeSerie().intValue());
        predio.setPredObjeIdentificador(row.getPrPredPcObjeIdentificador().longValue());

        if (row.getCampo1() != null) {
            predio.setCampo1(row.getCampo1());
        }
        if (row.getCampo2() != null) {
            predio.setCampo2(row.getCampo2());
        }
        if (row.getCampo3() != null) {
            predio.setCampo3(row.getCampo3());
        }
        if (row.getCampo4() != null) {
            predio.setCampo4(row.getCampo4());
        }
        if (row.getCampo5() != null) {
            predio.setCampo5(row.getCampo5());
        }
        if (row.getCampo6() != null) {
            predio.setCampo6(row.getCampo6());
        }
        if (row.getCampo7() != null) {
            predio.setCampo7(row.getCampo7());
        }
        if (row.getCampo8() != null) {
            predio.setCampo8(row.getCampo8());
        }
        if (row.getCampo9() != null) {
            predio.setCampo9(row.getCampo9());
        }
        if (row.getCampo10() != null) {
            predio.setCampo10(row.getCampo10());
        }
        if (row.getCampo11() != null) {
            predio.setCampo11(row.getCampo11());
        }
        if (row.getCampo12() != null) {
            predio.setCampo12(row.getCampo12());
        }
        if (row.getCampo13() != null) {
            predio.setCampo13(row.getCampo13());
        }
        if (row.getCampo14() != null) {
            predio.setCampo14(row.getCampo14());
        }
        if (row.getCampo15() != null) {
            predio.setCampo15(row.getCampo15());
        }
        if (row.getCampo16() != null) {
            predio.setCampo16(row.getCampo16());
        }
        if (row.getCampo17() != null) {
            predio.setCampo17(row.getCampo17());
        }
        if (row.getCampo18() != null) {
            predio.setCampo18(row.getCampo18());
        }
        if (row.getCampo19() != null) {
            predio.setCampo19(row.getCampo19());
        }
        if (row.getCampo20() != null) {
            predio.setCampo20(row.getCampo20());
        }

        predio.setCreadoPor(row.getCreadoPor());
        predio.setCreadoEl(new Date(row.getCreadoEl().getValue().getTime()));
        predio.setModificadoPor(row.getModificadoPor());
        predio.setModificadoEl(new Date(row.getCreadoEl().getValue().getTime()));

        return predio;
    }
    
  public static CaracteristicaPredioComponente cargaCaractPrediosComponentes(PrCaractPredComponentesVVORowImpl row) {
        CaracteristicaPredioComponente caractPredioComponente =
            new CaracteristicaPredioComponente();
    
    caractPredioComponente.setPrPredSerie(row.getPrPredSerie().intValue());
    caractPredioComponente.setPrPredIdentificador(row.getPrPredIdentificador().longValue());
    caractPredioComponente.setPrPrcoIdentificador(row.getPrPrcoIdentificador().intValue());
   // FIXME Enlazar vistas de detalles y caracteristicas para que al obtener row de detalle traiga su row de caracteristica
    // y cargarlo aqui
    caractPredioComponente.setValor1(row.getValor1());
    caractPredioComponente.setValor2(row.getValor2());
    caractPredioComponente.setValor3(row.getValor3());
    caractPredioComponente.setValor4(row.getValor4());
    caractPredioComponente.setEstatus(row.getEstatus());
    caractPredioComponente.setPrMopeSerie(row.getPrMopeSerie().intValue());
    caractPredioComponente.setPrMopeIdentificador(row.getPrMopeIdentificador().longValue());
    if (row.getCampo1() != null) {
        caractPredioComponente.setCampo1(row.getCampo1());
    }
    if (row.getCampo2() != null) {
        caractPredioComponente.setCampo2(row.getCampo2());
    }
    if (row.getCampo3() != null) {
        caractPredioComponente.setCampo3(row.getCampo3());
    }
    if (row.getCampo4() != null) {
        caractPredioComponente.setCampo4(row.getCampo4());
    }
    if (row.getCampo5() != null) {
        caractPredioComponente.setCampo5(row.getCampo5());
    }
    if (row.getCampo6() != null) {
        caractPredioComponente.setCampo6(row.getCampo6());
    }
    if (row.getCampo7() != null) {
        caractPredioComponente.setCampo7(row.getCampo7());
    }
    if (row.getCampo8() != null) {
        caractPredioComponente.setCampo8(row.getCampo8());
    }
    if (row.getCampo9() != null) {
        caractPredioComponente.setCampo9(row.getCampo9());
    }
    if (row.getCampo10() != null) {
        caractPredioComponente.setCampo10(row.getCampo10());
    }
    if (row.getCampo11() != null) {
        caractPredioComponente.setCampo11(row.getCampo11());
    }
    if (row.getCampo12() != null) {
        caractPredioComponente.setCampo12(row.getCampo12());
    }
    if (row.getCampo13() != null) {
        caractPredioComponente.setCampo13(row.getCampo13());
    }
    if (row.getCampo14() != null) {
        caractPredioComponente.setCampo14(row.getCampo14());
    }
    if (row.getCampo15() != null) {
        caractPredioComponente.setCampo15(row.getCampo15());
    }
    caractPredioComponente.setCreadoPor(row.getCreadoPor());
    caractPredioComponente.setCreadoEl(new Date(row.getCreadoEl().getValue().getTime()));
    caractPredioComponente.setModificadoPor(row.getModificadoPor());
    caractPredioComponente.setModificadoEl(new Date(row.getCreadoEl().getValue().getTime()));

    return caractPredioComponente;  
  }

    public static Predio cargaPredio(PrDatosPredioVVORowImpl row) {
      Predio predio = new Predio();
      predio.setClaveCatastral(row.getClaveCatastral());
      predio.setObjeSerie(row.getPcObjeSerie().intValue());
      predio.setObjeIdentificador(row.getPcObjeIdentificador().longValue());
      if (row.getNombrePredio() != null) {
      predio.setNombrePredio(row.getNombrePredio());
      }
      if (row.getDescripcionTerreno() != null) {
      predio.setDescripcionTerreno(row.getDescripcionTerreno());
      }
      predio.setPredioEnCondominio(row.getPredioEnCondominio());
      predio.setAnioAlta(row.getAnioAlta().intValue());
      predio.setPeriodoAlta(row.getPeriodoAlta().intValue());
      predio.setAnioVigencia(row.getAnioVigencia().intValue());
      predio.setPeriodoVigencia(row.getPeriodoVigencia().intValue());
      predio.setTipoPropiedad(row.getTipoPropiedad());
      predio.setTipoPredio(row.getTipoPredio());
      if (row.getFolioReal() != null) {
      predio.setFolioReal(row.getFolioReal());
      }
      if (row.getFechaInscripcionFolio() != null) {
      predio.setFechaInscripcionFolio(row.getFechaInscripcionFolio().dateValue());
      }
      if (row.getCurt() != null) {
      predio.setCurt(row.getCurt());
      }
      if (row.getNivel() != null) {
      predio.setNivel(row.getNivel());
      }
      if (row.getCentroideLatitud() != null) {
      predio.setCentroideLatitud(row.getCentroideLatitud());
      }
      if (row.getCentroideLongitud() != null) {
      predio.setCentroideLongitud(row.getCentroideLongitud());
      }
      predio.setProgramaActual(row.getProgramaActual());
      predio.setEstatus(row.getEstatus());
      if (row.getPeriodicidadAdeudo() != null) {
      predio.setPeriodicidadAdeudo(row.getPeriodicidadAdeudo().intValue());
      }
    
      return predio;
    }

    public static DomicilioGeografico cargaDomicilioGeografico(PcDomiciliosGeograficosVVORowImpl row) {
        DomicilioGeografico domicilio = new DomicilioGeografico();
        domicilio.setSerie(row.getSerie().intValue());
        domicilio.setIdentificador(row.getIdentificador().longValue());
        domicilio.setFechaAlta(row.getFechaAlta().dateValue());
        domicilio.setEstatus(row.getEstatus());
        domicilio.setTipoVialidad(row.getTipoVialidad());
        domicilio.setNombreVialidad(row.getNombreVialidad());
        domicilio.setNumeroExterior(row.getNumeroExterior());
        if (row.getLetra() != null) {
            domicilio.setLetra(row.getLetra());
        }
        if (row.getNumeroInterior() != null) {
            domicilio.setNumeroInterior(row.getNumeroInterior());
        }
        if (row.getExtensionNumeroInterior() != null) {
            domicilio.setExtensionNumeroInterior(row.getExtensionNumeroInterior());
        }
        if (row.getCodigoPostal() != null) {
            domicilio.setCodigoPostal(row.getCodigoPostal());
        }
        domicilio.setTipoAsentamiento(row.getTipoAsentamiento());
        domicilio.setNombreAsentamiento(row.getAsentamiento());
        domicilio.setNombreLocalidad(row.getLocalidad());
        domicilio.setMunicipio(row.getMunicipio());
        if (row.getTelefono1() != null) {
            domicilio.setTelefono1(row.getTelefono1());
        }
        if (row.getTelefono2() != null) {
            domicilio.setTelefono2(row.getTelefono2());
        }
        if (row.getTelefono3() != null) {
            domicilio.setTelefono3(row.getTelefono3());
        }
        if (row.getMedidorAgua() != null) {
            domicilio.setMedidorAgua(row.getMedidorAgua());
        }
        if (row.getMedidorLuz() != null) {
            domicilio.setMedidorLuz(row.getMedidorLuz());
        }
        if (row.getDescripcionUbicacion() != null) {
            domicilio.setDescripcionUbicacion(row.getDescripcionUbicacion());
        }
        domicilio.setPcObjeSerie(row.getPcObjeSerie().intValue());
        domicilio.setPcObjeIdentificador(row.getPcObjeIdentificador().longValue());
        if (row.getPcLocaIdentificador() != null) {
            domicilio.setPcLocaIdentificador(row.getPcLocaIdentificador().longValue());
        }
        if (row.getPcAsenIdentificador() != null) {
            domicilio.setPcAsenIdentificador(row.getPcAsenIdentificador().longValue());
        }
        if (row.getCampo1() != null) {
            domicilio.setCampo1(row.getCampo1());
        }
        if (row.getCampo2() != null) {
            domicilio.setCampo2(row.getCampo2());
        }
        if (row.getCampo3() != null) {
            domicilio.setCampo3(row.getCampo3());
        }
        if (row.getCampo4() != null) {
            domicilio.setCampo4(row.getCampo4());
        }
        if (row.getCampo5() != null) {
            domicilio.setCampo5(row.getCampo5());
        }
        if (row.getCampo6() != null) {
            domicilio.setCampo6(row.getCampo6());
        }
        if (row.getCampo7() != null) {
            domicilio.setCampo7(row.getCampo7());
        }
        if (row.getCampo8() != null) {
            domicilio.setCampo8(row.getCampo8());
        }
        if (row.getCampo9() != null) {
            domicilio.setCampo9(row.getCampo9());
        }
        if (row.getCampo10() != null) {
            domicilio.setCampo10(row.getCampo10());
        }
        if (row.getCampo11() != null) {
            domicilio.setCampo11(row.getCampo11());
        }
        if (row.getCampo12() != null) {
            domicilio.setCampo12(row.getCampo12());
        }
        if (row.getCampo13() != null) {
            domicilio.setCampo13(row.getCampo13());
        }
        if (row.getCampo14() != null) {
            domicilio.setCampo14(row.getCampo14());
        }
        if (row.getCampo15() != null) {
            domicilio.setCampo15(row.getCampo15());
        }

        return domicilio;
    }

    public static SujetoObjeto cargaSujetoObjeto(PcSujetosObjetosVVORowImpl row) {
      SujetoObjeto sujeto = new SujetoObjeto();
      sujeto.setSerie(row.getSerie().intValue());
      sujeto.setIdentificador(row.getIdentificador().longValue());
      sujeto.setObjeSerie(row.getPcObjeSerie().intValue());
      sujeto.setObjeIdentificador(row.getPcObjeIdentificador().longValue());
      if (row.getPcPefiNoPersona() != null) {
      sujeto.setPeFiNoPersona(row.getPcPefiNoPersona().longValue());
      //sujeto.setNoPersona(row.getPcPefiNoPersona().longValue());
      }
      if (row.getPcPemoNoPersona() != null) {
      sujeto.setPeMoNoPersona(row.getPcPemoNoPersona().longValue());
      //sujeto.setNoPersona(row.getPcPemoNoPersona().longValue());
      }
      sujeto.setRosuClave(row.getPcRosuClave().intValue());
      sujeto.setEstatus(row.getEstatus());
      sujeto.setFechaInicio(row.getFechaInicio().dateValue());
      if (row.getFechaFin() != null) {
      sujeto.setFechaFin(row.getFechaFin().dateValue());
      }
      if (row.getPorcentaje() != null) {
        sujeto.setPorcentaje(row.getPorcentaje().doubleValue());
      }
      sujeto.setOtrosDuenos(row.getOtrosDuenos());
      if (row.getCampo1() != null) {
      sujeto.setCampo1(row.getCampo1());
      }
      if (row.getCampo2() != null) {
      sujeto.setCampo2(row.getCampo2());
      }
      if (row.getCampo3() != null) {
      sujeto.setCampo3(row.getCampo3());
      }
    
      return sujeto;
    }

    public static DomicilioNotificar cargaDomicilioNotificar(PcDomicilioNotificarVVORowImpl row) {
        DomicilioNotificar domicilio = new DomicilioNotificar();
        if (row.getPcPefiNoPersona() != null) {
            domicilio.setPeFiNoPersona(row.getPcPefiNoPersona().longValue());
        }
        if (row.getPcPemoNoPersona() != null) {
            domicilio.setPrMoNoPersona(row.getPcPemoNoPersona().longValue());
        }
        domicilio.setTipoCalle(row.getTipoCalle());
        domicilio.setCalle(row.getCalle());
        domicilio.setNumeroExterior(row.getNumeroExterior());
        if (row.getLetra() != null) {
            domicilio.setLetra(row.getLetra());
        }
        if (row.getNumeroInterior() != null) {
            domicilio.setNumeroInterior(row.getNumeroInterior());
        }
        if (row.getCodigoPostal() != null) {
            domicilio.setCodigoPostal(row.getCodigoPostal());
        }
        domicilio.setColonia(row.getColonia());
        domicilio.setLocalidad(row.getLocalidad());
        if (row.getMunicipio() != null) {
            domicilio.setMunicipio(row.getMunicipio());
        }
        if (row.getEstado() != null) {
            domicilio.setEstado(row.getEstado());
        }
        if (row.getPais() != null) {
            domicilio.setPais(row.getPais());
        }
        if (row.getTelefono1() != null) {
            domicilio.setTelefono1(row.getTelefono1());
        }
        if (row.getTelefono2() != null) {
            domicilio.setTelefono2(row.getTelefono2());
        }
        if (row.getEmail() != null) {
            domicilio.setEmail(row.getEmail());
        }
        if (row.getComentario() != null) {
            domicilio.setComentario(row.getComentario());
        }
        domicilio.setEstatus(row.getEstatus());
                
        return domicilio;
    }
    
    public static CaracteristicaAsentamiento cargaCaracteristicaAsentamiento(PrCaractAsentamientosVORowImpl row) {
        CaracteristicaAsentamiento caractAsentamiento =
            new CaracteristicaAsentamiento();
    
    // FIXME Enlazar vistas de detalles y caracteristicas para que al obtener row de detalle traiga su row de caracteristica
     // y cargarlo aqui
    caractAsentamiento.setPcLocaIdentificador(row.getPcLocaIdentificador().longValue());
    caractAsentamiento.setPcAsenIdentificador(row.getPcAsenIdentificador().longValue());
    caractAsentamiento.setValor1(row.getValor1());
    if (row.getValor2() != null) {
      caractAsentamiento.setValor2(row.getValor2());
    }
    if (row.getValor3() != null) {
      caractAsentamiento.setValor3(row.getValor3());
    }
    if (row.getCampo1() != null) {
        caractAsentamiento.setCampo1(row.getCampo1());
    }
    if (row.getCampo2() != null) {
        caractAsentamiento.setCampo2(row.getCampo2());
    }
    if (row.getCampo3() != null) {
        caractAsentamiento.setCampo3(row.getCampo3());
    }
    if (row.getCampo4() != null) {
        caractAsentamiento.setCampo4(row.getCampo4());
    }
    if (row.getCampo5() != null) {
        caractAsentamiento.setCampo5(row.getCampo5());
    }
    if (row.getCampo6() != null) {
        caractAsentamiento.setCampo6(row.getCampo6());
    }
    if (row.getCampo7() != null) {
        caractAsentamiento.setCampo7(row.getCampo7());
    }
    if (row.getCampo8() != null) {
        caractAsentamiento.setCampo8(row.getCampo8());
    }
    if (row.getCampo9() != null) {
        caractAsentamiento.setCampo9(row.getCampo9());
    }
    if (row.getCampo10() != null) {
        caractAsentamiento.setCampo10(row.getCampo10());
    }
    if (row.getCampo11() != null) {
        caractAsentamiento.setCampo11(row.getCampo11());
    }
    if (row.getCampo12() != null) {
        caractAsentamiento.setCampo12(row.getCampo12());
    }
    if (row.getCampo13() != null) {
        caractAsentamiento.setCampo13(row.getCampo13());
    }
    if (row.getCampo14() != null) {
        caractAsentamiento.setCampo14(row.getCampo14());
    }
    if (row.getCampo15() != null) {
        caractAsentamiento.setCampo15(row.getCampo15());
    }
    caractAsentamiento.setCreadoPor(row.getCreadoPor());
    caractAsentamiento.setCreadoEl(new Date(row.getCreadoEl().getValue().getTime()));
    caractAsentamiento.setModificadoPor(row.getModificadoPor());
    caractAsentamiento.setModificadoEl(new Date(row.getCreadoEl().getValue().getTime()));

    return caractAsentamiento;
  }
}
