package mx.tgc.model.ro.ubicacion;

import mx.tgc.model.ro.ubicacion.common.AaUsuariosMunicipiosVVO;
import mx.tgc.utilityfwk.model.extension.view.GenericViewImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Fri Jul 03 15:49:08 MDT 2015
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class AaUsuariosMunicipiosVVOImpl extends GenericViewImpl implements AaUsuariosMunicipiosVVO {
    /**
     * This is the default constructor (do not remove).
     */
    public AaUsuariosMunicipiosVVOImpl() {
    }
}
