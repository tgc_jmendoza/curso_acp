package mx.tgc.model.listener;

import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

import javax.security.auth.Subject;

import javax.servlet.http.HttpServletRequest;

import mx.net.tgc.recaudador.resource.ParametroGeneral;
import mx.net.tgc.recaudador.reutil.parametrosgenerales.ParametrosGeneralesModuleImpl;

import mx.tgc.model.module.ConsultaAMImpl;
import mx.tgc.utilityfwk.resource.PublicKeys;

import weblogic.security.Security;


public class SecurityListener implements PhaseListener {
    public void afterPhase(PhaseEvent phaseEvent) {

    }

    public void beforePhase(PhaseEvent phaseEvent) {
        Subject sbj = Security.getCurrentSubject();
        String user = FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
        String host =
            ((HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRemoteHost();
        String ip =
            ((HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest()).getRemoteAddr();
        HttpServletRequest request =
            (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String endPoint = null;

        ParametroGeneral pgEndPoint = ParametrosGeneralesModuleImpl.getParametroGeneralXClaveInicializado("AA0008");
        if (pgEndPoint != null) {
            endPoint = pgEndPoint.getValor();
        }
        
        Map infoUser = ConsultaAMImpl.getInfoUserInicializado(user);

        infoUser.put(PublicKeys.LOGIN_USER, user);
        infoUser.put(PublicKeys.LOGIN_HOST, host);
        infoUser.put(PublicKeys.LOGIN_IP, ip);
        infoUser.put(PublicKeys.LOGIN_END_POINT, endPoint);
        
        sbj.getPublicCredentials().add(infoUser);
        weblogic.servlet.security.ServletAuthentication.runAs(sbj, request);

    }
    
    public PhaseId getPhaseId() {
        return PhaseId.RESTORE_VIEW;
    }
}
