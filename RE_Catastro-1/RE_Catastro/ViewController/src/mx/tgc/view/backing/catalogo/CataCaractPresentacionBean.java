package mx.tgc.view.backing.catalogo;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;

import mx.tgc.utilityfwk.resource.PropertiesReader;
import mx.tgc.utilityfwk.resource.PublicKeys;
import mx.tgc.utilityfwk.view.managed.GenericBean;

import oracle.adf.controller.v2.context.LifecycleContext;
import oracle.adf.view.rich.component.rich.RichPopup;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.data.RichTreeTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.context.AdfFacesContext;
import oracle.adf.view.rich.event.DialogEvent;
import oracle.adf.view.rich.event.PopupCanceledEvent;
import oracle.adf.view.rich.event.PopupFetchEvent;

import oracle.binding.AttributeBinding;
import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.JboException;
import oracle.jbo.Row;
import oracle.jbo.uicli.binding.JUCtrlHierBinding;
import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;

import org.apache.myfaces.trinidad.context.RequestContext;
import org.apache.myfaces.trinidad.event.ReturnEvent;
import org.apache.myfaces.trinidad.event.SelectionEvent;
import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.model.RowKeySet;
import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;


public class CataCaractPresentacionBean extends GenericBean {
  private RichPopup formaPopup;
  private RichInputText nomBloquePadre;
  private RichTable tablaPresentaciones;
  private RichTable tablaCaract;
  PropertiesReader properties;
  private RichTable tabla;
  private RichPopup popupCancelar;

  /**
   * This is the default constructor (do not remove).
   */
  public CataCaractPresentacionBean() {
    if (properties == null) {
      try {
        properties =
            new PropertiesReader("/mx/tgc/view/ViewControllerBundle.properties",
                                 CataCaractPresentacionBean.class);
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  public void setFormaPopup(RichPopup formaPopup) {
    this.formaPopup = formaPopup;
  }

  public RichPopup getFormaPopup() {
    return formaPopup;
  }

  public void setTablaPresentaciones(RichTable tablaPresentaciones) {
    this.tablaPresentaciones = tablaPresentaciones;
  }

  public RichTable getTablaPresentaciones() {
    return tablaPresentaciones;
  }

  public void setNomBloquePadre(RichInputText nomBloquePadre) {
    this.nomBloquePadre = nomBloquePadre;
  }

  public RichInputText getNomBloquePadre() {
    return nomBloquePadre;
  }

  public void setTablaCaract(RichTable tablaCaract) {
    this.tablaCaract = tablaCaract;
  }

  public RichTable getTablaCaract() {
    return tablaCaract;
  }

  @Override
  public void prepareModel(LifecycleContext lifecycleContext) {
    super.prepareModel(lifecycleContext);
    RequestContext context = RequestContext.getCurrentInstance();
    if (!context.isPostback()) {
      OperationBinding filtrar =
        (OperationBinding)getBindings().getControlBinding("filtrarCaractPresentaciones1");
      filtrar.execute();

      String debug = super.getParametroGeneral("AA0008");
      if (debug == null && debug.equals("")) {
        String msj = properties.getMessage("SYS.PARAMETRO_PR0008_NO_EXISTE");
        mostrarMensaje(FacesMessage.SEVERITY_ERROR, msj);
      }
    }
  }

  @Override
  public RichPanelGroupLayout getPanelBusquedaTreeTable() {
    RequestContext rc = RequestContext.getCurrentInstance();
    if (!rc.isPostback()) {
      this.setTituloPanelBusquedaTreeTable(properties.getMessage("TIT.SECCION_BUSQUEDA"));
      this.setTipPanelBusquedaTreeTable(properties.getMessage("TIP.BUSQUEDA_TREE_TABLE"));
      this.setBuscarPorTreeTable(properties.getMessage("LOV.BUSCAR_POR_TREE_TABLE"));
      this.setCriterioBusquedaTreeTable(properties.getMessage("LOV.CRITERIO_TREE_TABLE"));
      this.setParametroTreeTable(properties.getMessage("NOM.PARAM_TREE_TABLE"));
      this.setBotonBuscarTreeTable(properties.getMessage("BOTON.BUSCAR"));
      List<SelectItem> campos = new ArrayList<SelectItem>();
      //Estos valores se deben de ajustar por los campos deseados ///////
      campos.add(new SelectItem("Nombre", "Nombre"));
      campos.add(new SelectItem("Etiqueta", "Etiqueta"));
      setManagedBean("CataCaractPresentacionBean");
      setCamposDeBusquedaTreeTable(campos);
    }
    return super.getPanelBusquedaTreeTable();
  }

  public void filtrarCaracteristicas(int id) {
    try {
      BindingContainer bindings = this.getBindings();
      OperationBinding filtrarCatact =
        bindings.getOperationBinding("filtrarCaractPresentaciones");
      filtrarCatact.getParamsMap().put("idBloque", id);
      filtrarCatact.execute();
    } catch (Exception e) {
      throw new JboException(e.getMessage());
    }
  }

  /**
   * M�todo del bot�n �Aceptar� que confirma la acci�n de borrar antes de
   * borrar debe de validar que el registro est� disponible para ser borrado
   * @return
   */
  public String borrarRegistro() {
    BindingContainer bindings = getBindings();
    OperationBinding ob = bindings.getOperationBinding("Delete");
    ob.execute();
    getBindings();
    OperationBinding operationBinding2 =
      bindings.getOperationBinding("Commit");
    operationBinding2.execute();
    ob.getErrors();
    if (!ob.getErrors().isEmpty() ||
        !operationBinding2.getErrors().isEmpty()) {
      OperationBinding operationBinding3 =
        bindings.getOperationBinding("Rollback");
      operationBinding3.execute();
      mostrarMensaje(FacesMessage.SEVERITY_ERROR,
                     properties.getMessage("SYS.OPERACION_FALLIDA"));
      return null;
    } else {
      mostrarMensaje(FacesMessage.SEVERITY_INFO,
                     properties.getMessage("SYS.OPERACION_EXITOSA"));
      return null;
    }
  }

  public void formaPopUpFetchListener(PopupFetchEvent popupFetchEvent) {
    String clientId = popupFetchEvent.getLaunchSourceClientId();
    AttributeBinding lupa =
      (AttributeBinding)this.getBindings().getControlBinding("MostrarLupa");
    
    //Row row = rowTreeTable.getRow();
    AdfFacesContext fc = AdfFacesContext.getCurrentInstance();
    Map pfs = fc.getPageFlowScope();
    String operacion = pfs.get("operacion").toString();
    if (operacion != null && operacion.equals("nuevo")) {
        
        //RowTreeTable treeTable = this.getRowTreeTable();
        Row row=obtenerFilaSeleccionadaTT(this.getTreeTable());
        if (row == null) {
          this.getFormaPopup().cancel();
          mostrarMensaje(FacesMessage.SEVERITY_WARN,
                         properties.getMessage("PR_PRESENTACION_CARACTERISTICA.MANDATORY.PR_PRBA_IDENTIFICADOR"));

          return;
        }
        
      OperationBinding operationBinding =
        getBindings().getOperationBinding("CreateInsert");
      operationBinding.execute();
      lupa.setInputValue(true);
      oracle.adf.model.AttributeBinding ab =
        (oracle.adf.model.AttributeBinding)getBindings().getControlBinding("PrPrbaIdentificador1");
      ab.setInputValue(Integer.parseInt(row.getAttribute("Identificador").toString()));
      this.getNomBloquePadre().setValue(row.getAttribute("Nombre"));

    } else if (operacion != null && operacion.equals("modificar")) {
        //RowTreeTable rowTreeTable = this.getRowTreeTable();
        Row rowTablaBl=obtenerFilaSeleccionadaTT(this.getTreeTable());
        JUCtrlHierNodeBinding rowData = (JUCtrlHierNodeBinding)tablaPresentaciones.getSelectedRowData();
        Row row = rowData.getCurrentRow();
        if (row == null) {
          this.getFormaPopup().cancel();
          mostrarMensaje(FacesMessage.SEVERITY_WARN,
                         properties.getMessage("Por favor seleccione una caracter�stica."));

          return;
        }
        
      lupa.setInputValue(false);
      oracle.adf.model.AttributeBinding ab =
        (oracle.adf.model.AttributeBinding)getBindings().getControlBinding("PrPrbaIdentificador");   
      ab.setInputValue(Integer.parseInt(rowTablaBl.getAttribute("Identificador").toString()));
      this.getNomBloquePadre().setValue(rowTablaBl.getAttribute("Nombre"));
        AttributeBinding nombreCaract =
          (AttributeBinding)getBindings().getControlBinding("NombreCaract");
        nombreCaract.setInputValue(row.getAttribute("Nombre"));//Atributo de la tabla presentaciones
    }
  }

  public void editDialogListener(DialogEvent dialogEvent) {
    if (dialogEvent.getOutcome().name().equals("ok")) {
      commitOperation();
      refreshTreeIterator("PrPredBloquesAgrupacionesVO1Iterator");
      AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
      adfFacesContext.addPartialTarget(tablaPresentaciones);
    }
  }

  public void formaPopUpCancelarListener(PopupCanceledEvent popupCanceledEvent) {
    rollbackOperation();
    AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
    adfFacesContext.addPartialTarget(tablaPresentaciones);
  }

  public void CaracterPopUpFetchListener(PopupFetchEvent popupFetchEvent) {
    try {
      //Filtrar los tipos de aval�os que correspondan a la recaudaci�n base
      String muniId =
        super.getInfoUser().get(PublicKeys.LOGIN_MUNICIPIO_ID).toString();
      OperationBinding ob =
        this.getBindings().getOperationBinding("filtrarTiposAvaluos");
      ob.getParamsMap().put("muniId", muniId);
      ob.execute();
      //Hay que traerse la fila actual seleccionada
      //Esta se define al lanzar la forma de captura
      AttributeBinding bloque =
        (AttributeBinding)this.getBindings().getControlBinding("PrPrbaIdentificador1");
      //Luego se manda el filtrado de la tabla de caracter�sticas presentaciones
      OperationBinding ob2 =
        this.getBindings().getOperationBinding("filtrarCaractPresentaciones2");
      ob2.getParamsMap().put("prbaId", bloque.getInputValue().toString());
      ob2.execute();
    } catch (Exception e) {
      throw new JboException(e.getMessage());
    }
  }

  public void tiposAvaluosChangeListener(ValueChangeEvent valueChangeEvent) {
    try {
      //Filtrar la tabla de caracter�sticas presentaciones
      //Hay que traerse la fila actual seleccionada
      //Esta se define al lanzar la forma de captura
      AttributeBinding bloque =
        (AttributeBinding)this.getBindings().getControlBinding("PrPrbaIdentificador1");
      //Luego se manda el filtrado de la tabla de caracter�sticas presentaciones
      OperationBinding ob2 =
        this.getBindings().getOperationBinding("filtrarCaractPresentaciones2");
      ob2.getParamsMap().put("prbaId", bloque.getInputValue().toString());
      ob2.execute();
    } catch (Exception e) {
      throw new JboException(e.getMessage());
    }
  }

  private void commitOperation() {
    BindingContainer bindings = getBindings();
    OperationBinding ob = bindings.getOperationBinding("Commit");
    ob.execute();
    if (!ob.getErrors().isEmpty()) {
      mostrarMensaje(FacesMessage.SEVERITY_ERROR,
                     properties.getMessage("SYS.OPERACION_FALLIDA"));
    } else {
      mostrarMensaje(FacesMessage.SEVERITY_INFO,
                     properties.getMessage("SYS.OPERACION_EXITOSA"));
    }
  }

  private void rollbackOperation() {
    OperationBinding operationBinding =
      getBindings().getOperationBinding("Rollback");
    operationBinding.execute();
  }

  public void onRowSelection(SelectionEvent selectionEvent) {
    Row rw = obtenerFilaSeleccionadaTT(getTreeTable());
    filtrarCaracteristicas(Integer.parseInt(rw.getAttribute("Identificador").toString()));
    if (rw.getAttribute("Nivel") !=null){
        this.getPageFlowScope().put("nivelBloque", rw.getAttribute("Nivel"));
    }
  }

  /**
   * Invokes an expression
   * @param expr
   * @param returnType
   * @param argTypes
   * @param args
   * @return
   */
  public static Object invokeMethodExpression(String expr, Class returnType,
                                              Class[] argTypes,
                                              Object[] args) {
    FacesContext fc = FacesContext.getCurrentInstance();
    ELContext elctx = fc.getELContext();
    ExpressionFactory elFactory = fc.getApplication().getExpressionFactory();
    MethodExpression methodExpr =
      elFactory.createMethodExpression(elctx, expr, returnType, argTypes);
    return methodExpr.invoke(elctx, args);
  }

  /**
   * Invoke an expression
   * @param expr
   * @param returnType
   * @param argType
   * @param argument
   * @return
   */
  public static Object invokeMethodExpression(String expr, Class returnType,
                                              Class argType, Object argument) {
    return invokeMethodExpression(expr, returnType, new Class[] { argType },
                                  new Object[] { argument });
  }

  public void setTabla(RichTable tabla) {
    this.tabla = tabla;
  }

  public RichTable getTabla() {
    return tabla;
  }

  public void setPopupCancelar(RichPopup popupCancelar) {
    this.popupCancelar = popupCancelar;
  }

  public RichPopup getPopupCancelar() {
    return popupCancelar;
  }

  public void cancelarListener(ActionEvent actionEvent) {
    rollbackOperation();
    ocultaPopup();
  }

  public String ocultaPopup() {
    try {
      ExtendedRenderKitService erks =
        Service.getRenderKitService(FacesContext.getCurrentInstance(),
                                    ExtendedRenderKitService.class);
      StringBuilder strb =
        new StringBuilder("AdfPage.PAGE.findComponent(\"" + this.formaPopup.getClientId(FacesContext.getCurrentInstance()) +
                          "\").hide();");
      erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
    } catch (Exception e) {
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null,
                         new FacesMessage(FacesMessage.SEVERITY_INFO, "Error en operaci�n",
                                          null));
    }
    return null;
  }

  public void aceptarListener(ActionEvent actionEvent) {
    commitOperation();
    refreshTreeIterator("PrPredBloquesAgrupacionesVO1Iterator");
    ocultaPopup();
  }

  public void cancelarAction(ActionEvent actionEvent) {
      popupCancelar.hide();
    rollbackOperation();
    ocultaPopup();
  }

  public String muestraPopup() {
    try {
      ExtendedRenderKitService erks =
        Service.getRenderKitService(FacesContext.getCurrentInstance(),
                                    ExtendedRenderKitService.class);
      StringBuilder strb =
        new StringBuilder("AdfPage.PAGE.findComponent(\"" + this.formaPopup.getClientId(FacesContext.getCurrentInstance()) +
                          "\").show();");
      erks.addScript(FacesContext.getCurrentInstance(), strb.toString());
    } catch (Exception e) {
      FacesContext context = FacesContext.getCurrentInstance();
      context.addMessage(null,
                         new FacesMessage(FacesMessage.SEVERITY_INFO, "Error en operaci�n",
                                          null));
    }
    return null;
  }

  public void muestraPopListener(ActionEvent actionEvent) {
    muestraPopup();
  }

  /**
   * Manda llamar al popUp de caracter�sticas de valuaci�n.
   * @return String la transici�n a la que se mandar� el flujo
   * @author Isaac S�as Guti�rrez
   * @versi�n 1.0 04-01-2013
   * @component Cat�logo de caracter�sticas presentaci�n
   */
  public String lanzarPopUpCaractValuacion() {
    this.getPageFlowScope().put("catalogo", "false");

    //String nivelBloque = this.getPageFlowScope().get("nivelBloque").toString();
    String nivelBloque=null;
    if (this.getPageFlowScope().get("nivelBloque")!=null){
        this.getPageFlowScope().put("filtrarNivel", nivelBloque);
    }
    return "caractValuacion";
  }

  /**
   * Asigna a los bindings, que apuntan a la forma de captura, los valores
   * obtenidos en el popUp de caracter�sticas de valuaci�n.
   * @param returnEvent ReturnEvent evento por el cual se ejecuta este m�todo
   * @author Isaac S�as Guti�rrez
   * @versi�n 1.0 04-01-2013
   * @component Cat�logo de caracter�sticas presentaci�n
   */
   public void caractValuacionDialogListener(ReturnEvent returnEvent) {
       try {
           
           AttributeBinding pcPrcaIdentificador =
               (AttributeBinding)getBindings().getControlBinding("PrPrcaIdentificador");
           if (!returnEvent.getReturnParameters().isEmpty()) {
               Object serie = returnEvent.getReturnParameters().get("serie");
               Object identificador = returnEvent.getReturnParameters().get("identificador");
               Object nombre = returnEvent.getReturnParameters().get("nombre");
               AttributeBinding prpbaIdentificador = (AttributeBinding)getBindings().getControlBinding("PrPrbaIdentificador1");
               BindingContainer bindings = getBindings();
               OperationBinding ob = bindings.getOperationBinding("validaCaract");
               ob.getParamsMap().put("prcaSerie", serie);
               ob.getParamsMap().put("prcaId", identificador);
               ob.getParamsMap().put("prba", prpbaIdentificador.getInputValue());
               ob.execute();
               if (Boolean.parseBoolean((String)ob.getResult()) == false) {
                   pcPrcaIdentificador.setInputValue(null);
                   mostrarMensaje(FacesMessage.SEVERITY_ERROR, properties.getMessage("SYS.BLOQUE_TIENE_CARACTERISTICA_DUPLICADA"));
               } else {
                   AttributeBinding pcPrcaSerie = (AttributeBinding)getBindings().getControlBinding("PrPrcaSerie");
                   pcPrcaSerie.setInputValue(serie);
                   pcPrcaIdentificador.setInputValue(identificador);
                   AttributeBinding nombreCaract = (AttributeBinding)getBindings().getControlBinding("NombreCaract");
                   nombreCaract.setInputValue(nombre);
               }
           }
       } 
       catch (Exception e) {
           mostrarMensaje(FacesMessage.SEVERITY_ERROR, e.getLocalizedMessage());
       }
   }
  
    /**
     * Fecha:12/05/2015
     * Origen:RE - Catastro - MR0017_1 - DGO - Cat�logo de segmentos catastrales - Se desaparecen los botones al querer agregar un hijo de tercer nivel.
     * Autor:Brenda Gomez.
     * */
    public Row obtenerFilaSeleccionadaTT(RichTreeTable nombreTT) {
        Row row = null;
        JUCtrlHierNodeBinding node = obtenerNodoSeleccionadoTT(nombreTT);
        if (node != null) {
            row = node.getRow();
        }
        return row;
    }

    /**
     * Fecha:12/05/2015
     * Origen:RE - Catastro - MR0017_1 - DGO - Cat�logo de segmentos catastrales - Se desaparecen los botones al querer agregar un hijo de tercer nivel.
     * Autor:Brenda Gomez.
     * */
    public JUCtrlHierNodeBinding obtenerNodoSeleccionadoTT(RichTreeTable nombreTT) {
        RowKeySet rks = nombreTT.getSelectedRowKeys();
        JUCtrlHierNodeBinding nodo = null;
        if (rks != null) {
            CollectionModel treeModel = (CollectionModel)nombreTT.getValue();
            JUCtrlHierBinding treeBinding = (JUCtrlHierBinding)treeModel.getWrappedData();
            List firstSet = (List)rks.iterator().next();
            nodo = treeBinding.findNodeByKeyPath(firstSet);
        }
        return nodo;
    }

}
