package mx.tgc.model.module.view.PrPredialCaracteristicasVO1VO;

import java.sql.SQLException;
import java.sql.Statement;

import mx.tgc.model.module.CataCaracteristicasValuacionAMImpl;
import mx.tgc.model.module.applicationModule.CataCaracteristicasValuacionAMFixture;

import oracle.jbo.Row;
import oracle.jbo.ViewObject;

import org.junit.*;
import static org.junit.Assert.*;

public class PrPredialCaracteristicasVO1VOTest {
    private static CataCaracteristicasValuacionAMFixture fixture1 =
        CataCaracteristicasValuacionAMFixture.getInstance();
    private static CataCaracteristicasValuacionAMImpl _amImpl =
        (CataCaracteristicasValuacionAMImpl)fixture1.getApplicationModule();

    public PrPredialCaracteristicasVO1VOTest() {
    }

    @Test
    public void testAccess() {
        ViewObject view =
            fixture1.getApplicationModule().findViewObject("PrPredialCaracteristicasVO1");
        assertNotNull(view);
    }

    /**
     * M�todo que sirve para probar los campos obligatorios
     * verifica que efectivamente si no se llenan los campos obligatorios
     * al validar el nuevo registro debe arrojar error.
     *
     * @author: Ang�lica Ledezma
     * @since 15-sept-2011
     */
    @Test
    public void testCamposObligatorios() {
        ViewObject view =
            _amImpl.findViewObject("PrPredialCaracteristicasVO1");
        Row row = view.createRow();

        //TODO: por cada atributo obligatorio en la vista implementar lo siguiente:
        //NOTA: los campos creado_el, creado_por, etc. si se configuraron adecuadamente en la
        //      entidad para llenarse de forma autom�tica no es necesario llenarlos
        //      en este m�todo.
        row.setAttribute("Serie", 2100);
        row.setAttribute("Identificador", 9999999999L);
        row.setAttribute("Nombre", "PRUEBAPRUEBAPRUEBA");
        row.setAttribute("Etiqueta", "PRUEBAJUNIT123");
        row.setAttribute("TipoDeDato", "CA");
        row.setAttribute("LongitudMaxima", 2);
        row.setAttribute("LongitudDespliegue", 3);
        row.setAttribute("TipoDeValidacion", "AL");
        row.setAttribute("Requerida", "S");
        row.setAttribute("Estatus", "AC");
        row.setAttribute("Orden", 1);
        row.setAttribute("Comportamiento", "A");
        row.setAttribute("Nivel", "C");
        row.setAttribute("PrTiavSerie", 2011);
        row.setAttribute("PrTiavIdentificador", 200);

        //-------------
        row.validate();
        _amImpl.getDBTransaction().rollback();
    }

    /**
     * M�todo utilizado para probar la longitud de los campos
     * de una vista de datos, el m�todo prueba que las longitudes
     * sean probadas con la m�xima longitud permitida para los
     * campos de tipo String (VARCHAR2).
     *
     * @author: Ang�lica Ledezma
     * @since 15-SEP-2011
     */
    @Test
    public void testPresicionMaximaEnCampos() {
        ViewObject view =
            _amImpl.findViewObject("PrPredialCaracteristicasVO1");
        Row row = view.createRow();

        //TODO: variables predefinidas con varias longitudes para
        //probar el m�ximo de caracteres permitidos en los campos de las tablas

        String campo4Chars = "";
        for (int i = 0; i < 4; i++) {
            campo4Chars += "1";
        }

        String campo15Chars = "";
        for (int i = 0; i < 15; i++) {
            campo15Chars += "1";
        }

        String campo100Chars = "";
        for (int i = 0; i < 100; i++) {
            campo100Chars += "1";
        }

        String campo30Chars = "";
        for (int i = 0; i < 30; i++) {
            campo30Chars += "1";
        }

        String campo2Chars = "";
        for (int i = 0; i < 2; i++) {
            campo2Chars += "1";
        }

        String campo11Chars = "";
        for (int i = 0; i < 11; i++) {
            campo11Chars += "1";
        }

        String campo1Chars = "";
        for (int i = 0; i < 1; i++) {
            campo1Chars += "1";
        }

        String campo6Chars = "";
        for (int i = 0; i < 6; i++) {
            campo6Chars += "1";
        }

        String campo2000Chars = "";
        for (int i = 0; i < 2000; i++) {
            campo2000Chars += "1";
        }

        //TODO: por cada atributo en la vista implementar lo siguiente:
        //NOTA: los campos creado_el, creado_por, etc. si se configuraron adecuadamente en la
        //      entidad para llenarse de forma autom�tica no es necesario llenarlos
        //      en este m�todo.
        row.setAttribute("Serie", Integer.parseInt(campo4Chars));
        row.setAttribute("Identificador", Long.parseLong(campo15Chars));
        row.setAttribute("Nombre", campo100Chars);
        row.setAttribute("Etiqueta", campo30Chars);
        row.setAttribute("TipoDeDato", campo2Chars);
        row.setAttribute("LongitudMaxima", Long.parseLong(campo11Chars));
        row.setAttribute("LongitudDespliegue", Long.parseLong(campo11Chars));
        row.setAttribute("TipoDeValidacion", campo2Chars);
        row.setAttribute("Requerida", campo1Chars);
        row.setAttribute("Estatus", campo2Chars);
        row.setAttribute("Orden", Integer.parseInt(campo6Chars));
        row.setAttribute("Comportamiento", campo1Chars);
        row.setAttribute("Nivel", campo1Chars);
        row.setAttribute("PrTiavSerie", Integer.parseInt(campo4Chars));
        row.setAttribute("PrTiavIdentificador", Long.parseLong(campo15Chars));
        row.setAttribute("Campo1", campo2000Chars);
        row.setAttribute("Campo2", campo2000Chars);
        row.setAttribute("Campo3", campo2000Chars);
        row.setAttribute("Campo4", campo2000Chars);
        row.setAttribute("Campo5", campo2000Chars);
        row.setAttribute("Campo6", campo2000Chars);
        row.setAttribute("Campo7", campo2000Chars);
        row.setAttribute("Campo8", campo2000Chars);
        row.setAttribute("Campo9", campo2000Chars);
        row.setAttribute("Campo10", campo2000Chars);
        row.setAttribute("Campo11", campo2000Chars);
        row.setAttribute("Campo12", campo2000Chars);
        row.setAttribute("Campo13", campo2000Chars);
        row.setAttribute("Campo14", campo2000Chars);
        row.setAttribute("Campo15", campo2000Chars);
        row.validate();
        _amImpl.getDBTransaction().rollback();
    }

    /**
     * M�todo utilizado para probar altas y bajas de registros en la vista de datos
     * se crea un nuevo row al cual se le llena con datos de prueba para poder
     * probar las operaciones de insert, update y delete.
     *
     * @author: Ang�lica Ledezma
     * @since 15-SEP-2011
     */
    @Test
    public void testInsertUpdateDelete() {
        ViewObject view =
            _amImpl.findViewObject("PrPredialCaracteristicasVO1");
        Row row = view.createRow();
        
        //TODO: Llenar cada atributo de la vista con valores para probar los inserts y deletes
        //NOTA: los campos creado_el, creado_por, etc. si se configuraron adecuadamente en la
        //      entidad para llenarse de forma autom�tica no es necesario llenarlos
        //      en este m�todo.

        row.setAttribute("Serie", 2100);
        row.setAttribute("Identificador", 9999999999L);
        row.setAttribute("Nombre", "PRUEBAPRUEBAPRUEBA");
        row.setAttribute("Etiqueta", "PRUEBAJUNIT123");
        row.setAttribute("TipoDeDato", "CA");
        row.setAttribute("LongitudMaxima", 2);
        row.setAttribute("LongitudDespliegue", 3);
        row.setAttribute("TipoDeValidacion", "AL");
        row.setAttribute("Requerida", "S");
        row.setAttribute("Estatus", "AC");
        row.setAttribute("Orden", 1);
        row.setAttribute("Comportamiento", "A");
        row.setAttribute("Nivel", "C");
        row.setAttribute("PrTiavSerie", 2011);
        row.setAttribute("PrTiavIdentificador", 104);

        _amImpl.getDBTransaction().commit();

        row.setAttribute("Serie", 2111);
        row.setAttribute("Identificador", 200);
        row.setAttribute("Nombre", "NOMBREPRUEBAJU");
        row.setAttribute("Etiqueta", "PJUNIT");
        row.setAttribute("TipoDeDato", "CA");
        row.setAttribute("LongitudMaxima", 3);
        row.setAttribute("LongitudDespliegue", 4);
        row.setAttribute("TipoDeValidacion", "AL");
        row.setAttribute("Requerida", "S");
        row.setAttribute("Estatus", "IN");
        _amImpl.getDBTransaction().commit();

        row.remove();
        _amImpl.getDBTransaction().commit();

    }


    @BeforeClass
    public static void setUp() {
        Statement stmt = _amImpl.getDBTransaction().createStatement(1);
        try {
            stmt.executeUpdate("INSERT INTO CC_RECAUDACIONES (IDENTIFICADOR,CLAVE,NOMBRE,ESTATUS,DOMICILIO,RECAUDADOR,CC_RECA_IDENTIFICADOR,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5,OPERACION)" +
                               "VALUES (200223,'022223','TGCIANOS PRUEBAS JUNIT3','AC',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'VN')");
            stmt.executeUpdate("INSERT INTO PR_TIPOS_AVALUOS (SERIE,IDENTIFICADOR,TIPO_AVALUO,NOMBRE,DESCRIPCION,ESTATUS,FECHA_INICIO,FECHA_FIN,CC_RECA_IDENTIFICADOR,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5,CAMPO6,CAMPO7,CAMPO8,CAMPO9,CAMPO10,CAMPO11,CAMPO12,CAMPO13,CAMPO14,CAMPO15,CREADO_POR,CREADO_EL,MODIFICADO_POR,MODIFICADO_EL) " +
                               "VALUES (2222,200,'PP','PRUEBA LOCA','TU LOCA','IN',to_timestamp('08-SEP-11','DD-MON-RR HH.MI.SSXFF AM'),to_timestamp('08-SEP-11','DD-MON-RR HH.MI.SSXFF AM'),200223,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'RECAUDADOR',to_timestamp('08-SEP-11','DD-MON-RR HH.MI.SSXFF AM'),'RECAUDADOR',to_timestamp('08-SEP-11','DD-MON-RR HH.MI.SSXFF AM'))");
            _amImpl.getDBTransaction().commit();
        } catch (SQLException ex) {
            ex.printStackTrace();
            _amImpl.getDBTransaction().rollback();
        }
    }

    @AfterClass
    public static void tearDown() {
        Statement stmt = _amImpl.getDBTransaction().createStatement(1);
        try {
            stmt.execute("delete from cc_recaudaciones where identificador = 200223");
            stmt.execute("delete from pr_tipos_avaluos where serie = 2222");
            _amImpl.getDBTransaction().commit();
        } catch (SQLException ex) {
            ex.printStackTrace();
            _amImpl.getDBTransaction().rollback();
        }
    }
}
