package mx.tgc.model.module.view.PrGirosVO2VO;

import mx.tgc.model.module.applicationModule.CataUsoSueloAMFixture;

import oracle.jbo.ViewObject;

import org.junit.*;
import static org.junit.Assert.*;

public class PrGirosVO2VOTest {
    private CataUsoSueloAMFixture fixture1 = CataUsoSueloAMFixture.getInstance();

    public PrGirosVO2VOTest() {
    }

    @Test
    public void testAccess() {
        ViewObject view = fixture1.getApplicationModule().findViewObject("PrGirosVO2");
        assertNotNull(view);
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
}
