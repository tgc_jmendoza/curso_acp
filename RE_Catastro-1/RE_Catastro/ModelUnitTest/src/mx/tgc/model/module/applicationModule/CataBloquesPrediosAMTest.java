package mx.tgc.model.module.applicationModule;

import java.sql.SQLException;
import java.sql.Statement;

import mx.tgc.model.module.CataBloquesPrediosAMImpl;

import mx.tgc.model.view.movimiento.PrPredBloquesAgrupacionesVOImpl;

import mx.tgc.model.view.movimiento.PrPresentacionCaracteristicaVOImpl;

import org.junit.*;
import static org.junit.Assert.*;

public class CataBloquesPrediosAMTest {
    private static CataBloquesPrediosAMFixture fixture1 =
        CataBloquesPrediosAMFixture.getInstance();
    private static CataBloquesPrediosAMImpl _amImpl =
        (CataBloquesPrediosAMImpl)fixture1.getApplicationModule();

    public CataBloquesPrediosAMTest() {
    }

    /**
     * M�todo usado para inicializar todo lo necesario para las pruebas.
     *
     * @author Isaac S�as Guti�rrez
     * @since 20-SEP-2011
     */
    @BeforeClass
    public static void setUp() {
        insertFiltrarBloquesPredios();
        insertFiltrarCaractPresentaciones();
    }

    /**
     * M�todo usado para borrar todo lo creado para las pruebas.
     *
     * @author Isaac S�as Guti�rrez
     * @since 20-SEP-2011
     */
    @AfterClass
    public static void tearDown() {
        deleteFiltrarCaractPresentaciones();
        deleteFiltrarBloquesPredios();
    }

    @Test
    public void testFiltrarPorRecaudacion() {
    }

    /**
     * M�todo usado para probar filtrarCaractPresentaciones.
     * Ejecuta el m�todo, luego se cuenta el n�mero de registros que deja el
     * filtro, de obtener diferente n�mero del esperado, devolvemos un mensaje
     * indicando cual fue el resultado.
     *
     * @author Isaac S�as Guti�rrez
     * @since 22-SEP-2011
     */
    @Test
    public void testFiltrarCaractPresentaciones() {
        _amImpl.filtrarCaractPresentaciones(new oracle.jbo.domain.Number(999999999999999L));
        PrPresentacionCaracteristicaVOImpl view =
            _amImpl.getPrPresentacionCaracteristicaVO1();
        int cantidadCaracteristicas = view.getRowCount();
        assertTrue("La cantidad esperada de bloques es 1 y se recibieron: " +
                   cantidadCaracteristicas, cantidadCaracteristicas == 1);
    }

    /**
     * M�todo usado para probar buscarCaractPresentaciones.
     * Ejecuta el m�todo, luego se cuenta el n�mero de registros que devuelve la
     * b�squeda, de obtener diferente n�mero del esperado, devolvemos un mensaje
     * indicando cual fue el resultado.
     *
     * @author Isaac S�as Guti�rrez
     * @since 22-SEP-2011
     */
    @Test
    public void testBuscarCaractPresentaciones() {
        _amImpl.buscarCaractPresentaciones("NOMBRE", null);
        PrPresentacionCaracteristicaVOImpl view =
            _amImpl.getPrPresentacionCaracteristicaVO1();
        int cantidadCaracteristicas = view.getRowCount();
        assertTrue("La cantidad esperada de bloques es 1 y se recibieron: " +
                   cantidadCaracteristicas, cantidadCaracteristicas == 1);
    }

    /**
     * M�todo usado para probar validaCaract.
     * Ejecuta el m�todo, luego se cuenta el n�mero de registros que deja el
     * filtro, de obtener diferente n�mero del esperado, devolvemos un mensaje
     * indicando cual fue el resultado.
     *
     * @author Isaac S�as Guti�rrez
     * @since 22-SEP-2011
     */
    @Test
    public void testValidaCaract() {
        _amImpl.validaCaract("9999", "999999999999999", "999999999999999");
        PrPresentacionCaracteristicaVOImpl view =
            _amImpl.getPrPresentacionCaracteristicaVO2();
        int cantidadCaracteristicas = view.getRowCount();
        assertTrue("La cantidad esperada de bloques es 1 y se recibieron: " +
                   cantidadCaracteristicas, cantidadCaracteristicas == 1);
    }

    /**
     * M�todo usado para probar filtrarBloquesPredios.
     * Ejecuta el m�todo, luego se cuenta el n�mero de registros que deja el
     * filtro, de obtener diferente n�mero del esperado, devolvemos un mensaje
     * indicando cual fue el resultado.
     *
     * @author Isaac S�as Guti�rrez
     * @since 20-SEP-2011
     */
    @Test
    public void testFiltrarBloquesPredios() {
        _amImpl.filtrarBloquesPredios("999999999999999");
        PrPredBloquesAgrupacionesVOImpl view =
            _amImpl.getPrPredBloquesAgrupacionesVO2();
        int cantidadBloques = view.getRowCount();
        assertTrue("La cantidad esperada de bloques es 1 y se recibieron: " +
                   cantidadBloques, cantidadBloques == 1);
    }

    /**
     * M�todo usado para la prueba unitaria de testFiltrarBloquesPredios, y que
     * inserta los registros necesarios para la prueba.
     *
     * @author Isaac S�as Guti�rrez
     * @since 20-SEP-2011
     */
    private static void insertFiltrarBloquesPredios() {
        Statement stmt = _amImpl.getDBTransaction().createStatement(1);
        try {
            stmt.execute("Insert into PR_PRED_BLOQUES_AGRUPACIONES (IDENTIFICADOR,NOMBRE,ETIQUETA,ORDEN,PANTALLA,PR_PRBA_IDENTIFICADOR,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5,CAMPO6,CAMPO7,CAMPO8,CAMPO9,CAMPO10,CAMPO11,CAMPO12,CAMPO13,CAMPO14,CAMPO15,CREADO_POR,CREADO_EL,MODIFICADO_POR,MODIFICADO_EL) values (999999999999999,'NOMBRE','ETIQUETA',123456789,123456789,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,'RECAUDADOR',to_timestamp('09/09/11','DD/MM/RR HH24:MI:SSXFF'),'RECAUDADOR',to_timestamp('09/09/11','DD/MM/RR HH24:MI:SSXFF'))");
            _amImpl.getDBTransaction().commit();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            _amImpl.getDBTransaction().rollback();
        }
    }

    /**
     * M�todo usado para la prueba unitaria de testFiltrarBloquesPredios, y que
     * borra todo lo creado para la prueba.
     *
     * @author Isaac S�as Guti�rrez
     * @since 20-SEP-2011
     */
    private static void deleteFiltrarBloquesPredios() {
        Statement stmt = _amImpl.getDBTransaction().createStatement(1);
        try {
            stmt.execute("delete from pr_pred_bloques_agrupaciones where identificador = 999999999999999");
            _amImpl.getDBTransaction().commit();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            _amImpl.getDBTransaction().rollback();
        }
    }

    /**
     * M�todo usado para la prueba unitaria de testFiltrarCaractPresentaciones,
     * y que inserta los registros necesarios para la prueba.
     *
     * @author Isaac S�as Guti�rrez
     * @since 22-SEP-2011
     */
    private static void insertFiltrarCaractPresentaciones() {
        Statement stmt = _amImpl.getDBTransaction().createStatement(1);
        try {
            stmt.execute("Insert into PR_TIPOS_AVALUOS (SERIE,IDENTIFICADOR,TIPO_AVALUO,NOMBRE,DESCRIPCION,ESTATUS,FECHA_INICIO,FECHA_FIN,CC_RECA_IDENTIFICADOR,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5,CAMPO6,CAMPO7,CAMPO8,CAMPO9,CAMPO10,CAMPO11,CAMPO12,CAMPO13,CAMPO14,CAMPO15,CREADO_POR,CREADO_EL,MODIFICADO_POR,MODIFICADO_EL) values (9999,999999999999999,'TIPOAV','NOMBRE','DESCRIPCION','AC',to_timestamp('20/09/11','DD/MM/RR HH24:MI:SSXFF'),null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,'RECAUDADOR',to_timestamp('08/09/11','DD/MM/RR HH24:MI:SSXFF'),'RECAUDADOR',to_timestamp('08/09/11','DD/MM/RR HH24:MI:SSXFF'))");
            stmt.execute("Insert into PR_PREDIAL_CARACTERISTICAS (SERIE,IDENTIFICADOR,NOMBRE,ETIQUETA,TIPO_DE_DATO,LONGITUD_MAXIMA,LONGITUD_DESPLIEGUE,TIPO_DE_VALIDACION,REQUERIDA,ESTATUS,ORDEN,COMPORTAMIENTO,NIVEL,PR_TIAV_SERIE,PR_TIAV_IDENTIFICADOR,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5,CAMPO6,CAMPO7,CAMPO8,CAMPO9,CAMPO10,CAMPO11,CAMPO12,CAMPO13,CAMPO14,CAMPO15,CREADO_POR,CREADO_EL,MODIFICADO_POR,MODIFICADO_EL) values (9999,999999999999999,'NOMBRE','ETIQUETA','CA',12345678910,12345678910,'AL','S','AC',1,'A','1',9999,999999999999999,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,'RECAUDADOR',to_timestamp('20/09/11','DD/MM/RR HH24:MI:SSXFF'),'RECAUDADOR',to_timestamp('20/09/11','DD/MM/RR HH24:MI:SSXFF'))");
            stmt.execute("Insert into PR_PRESENTACION_CARACTERISTICA (PR_PRCA_SERIE,PR_PRCA_IDENTIFICADOR,PR_PRBA_IDENTIFICADOR,ORDEN,CAPTURABLE,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5,CAMPO6,CAMPO7,CAMPO8,CAMPO9,CAMPO10,CAMPO11,CAMPO12,CAMPO13,CAMPO14,CAMPO15,CREADO_POR,CREADO_EL,MODIFICADO_POR,MODIFICADO_EL) values (9999,999999999999999,999999999999999,1,'S',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,'anonymous',to_timestamp('22/09/11','DD/MM/RR HH24:MI:SSXFF'),'anonymous',to_timestamp('22/09/11','DD/MM/RR HH24:MI:SSXFF'))");
            _amImpl.getDBTransaction().commit();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            _amImpl.getDBTransaction().rollback();
        }
    }

    /**
     * M�todo usado para la prueba unitaria de testFiltrarCaractPresentaciones,
     * y que borra todo lo creado para la prueba.
     *
     * @author Isaac S�as Guti�rrez
     * @since 22-SEP-2011
     */
    private static void deleteFiltrarCaractPresentaciones() {
        Statement stmt = _amImpl.getDBTransaction().createStatement(1);
        try {
            stmt.execute("delete from pr_presentacion_caracteristica where pr_prca_serie = 9999 and pr_prca_identificador = 999999999999999 and pr_prba_identificador = 999999999999999");
            stmt.execute("delete from pr_predial_caracteristicas where serie = 9999 and identificador = 999999999999999");
            stmt.execute("delete from pr_tipos_avaluos where identificador = 999999999999999");
            _amImpl.getDBTransaction().commit();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
            _amImpl.getDBTransaction().rollback();
        }
    }
}
