package mx.tgc.model.module.view.PrConfiguracionesElementosVO1VO;

import java.sql.SQLException;
import java.sql.Statement;

import mx.tgc.model.module.CatalogoSencilloAMImpl;
import mx.tgc.model.module.applicationModule.CatalogoSencilloAMFixture;

import oracle.jbo.ApplicationModule;
import oracle.jbo.Row;
import oracle.jbo.ViewObject;
import oracle.jbo.server.DBTransaction;

import org.junit.AfterClass;
import static org.junit.Assert.assertNotNull;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * PrConfiguracionesElementosVO1VOTest
 *
 * Clase de prueba para la vista de PrConfiguracionesElementosVO1 contenida en
 * el m�dulo <code>mx.tgc.model.module.CatalogoSencilloAMImpl</code>
 *
 * Se implementan 4 m�todos para revisar:
 *
 * - Creacion de instancia de la vista.
 * - Campos m�nimos requeridos.
 * - Presici�n m�xima de campos
 * - InsertUpdateDelete de registros
 *
 * @author: Jorge Hern�ndez
 * @since: 15-SEP-2011
 *
 */
public class PrConfiguracionesElementosVO1VOTest {
    private static CatalogoSencilloAMFixture fixture1 =
        CatalogoSencilloAMFixture.getInstance();

    public PrConfiguracionesElementosVO1VOTest() {
    }

    /**
     * M�todo que sirve para probar si la vista se inicializa correctamente para
     * aceptar peticiones de altas, bajas, cambios y consultas.
     *
     * @author: Jorge Hern�ndez
     * @since: 15-SEP-2011
     */
    @Test
    public void testCreacionInstancia() {
        ViewObject view =
            fixture1.getApplicationModule().findViewObject("PrConfiguracionesElementosVO1");
        assertNotNull(view);
    }

    /**
     * M�todo que sirve para probar los campos obligatorios verifica que
     * efectivamente si no se llenan los campos obligatorios al validar el
     * nuevo registro debe arrojar error.
     *
     * @author: Jorge Hern�ndez
     * @since: 15-SEP-2011
     */
    @Test
    public void testCamposObligatorios() {
        CatalogoSencilloAMImpl am =
            (CatalogoSencilloAMImpl)fixture1.getApplicationModule();
        ViewObject view =
            am.findViewObject("PrConfiguracionesElementosVO1");
        Row row = view.createRow();
        row.setAttribute("PrTmprSerie", 2100);
        row.setAttribute("PrTmprIdentificador", 1);
        row.setAttribute("PrElemSerie", 2100);
        row.setAttribute("PrElemIdentificador", 1);
        row.setAttribute("Orden", 1);
        row.setAttribute("Modificable", "S");
        row.validate();
        am.getDBTransaction().rollback();
    }

    /**
     * M�todo utilizado para probar la longitud de los campos
     * de una vista de datos, el m�todo prueba que las longitudes
     * sean probadas con la m�xima longitud permitida para los
     * campos de tipo String (VARCHAR2).
     *
     * @author Jorge Hern�ndez
     * @since 15-SEP-2011
     */
    @Test
    public void testPresicionMaximaEnCampos() {
        CatalogoSencilloAMImpl am =
            (CatalogoSencilloAMImpl)fixture1.getApplicationModule();
        ViewObject view =
            am.findViewObject("PrConfiguracionesElementosVO1");
        Row row = view.createRow();

        String campo2000Chars = "";
        for (int i = 0; i < 2000; i++) {
            campo2000Chars += "X";
        }

        row.setAttribute("PrTmprSerie", 9999);
        row.setAttribute("PrTmprIdentificador", 999999999999999L);
        row.setAttribute("PrElemSerie", 9999);
        row.setAttribute("PrElemIdentificador", 999999999999999L);
        row.setAttribute("Orden", 12345);
        row.setAttribute("Modificable", "S");
        row.setAttribute("Campo1", campo2000Chars);
        row.setAttribute("Campo2", campo2000Chars);
        row.setAttribute("Campo3", campo2000Chars);
        row.setAttribute("Campo4", campo2000Chars);
        row.setAttribute("Campo5", campo2000Chars);
        row.setAttribute("Campo6", campo2000Chars);
        row.setAttribute("Campo7", campo2000Chars);
        row.setAttribute("Campo8", campo2000Chars);
        row.setAttribute("Campo9", campo2000Chars);
        row.setAttribute("Campo10", campo2000Chars);
        row.setAttribute("Campo11", campo2000Chars);
        row.setAttribute("Campo12", campo2000Chars);
        row.setAttribute("Campo13", campo2000Chars);
        row.setAttribute("Campo14", campo2000Chars);
        row.setAttribute("Campo15", campo2000Chars);

        row.validate();
        am.getDBTransaction().rollback();
    }

    /**
     * M�todo utilizado para probar altas y
     * bajas de registros en la vista de datos
     * se crea un nuevo row al cual se le llena con datos
     * de prueba para poder probar las operaciones de
     * insert y delete.
     *
     * @author Jorge Hern�ndez
     * @since 12-SEP-2011
     */
    @Test
    public void testInsertUpdateDelete() {
        CatalogoSencilloAMImpl am =
            (CatalogoSencilloAMImpl)fixture1.getApplicationModule();
        ViewObject view =
            am.findViewObject("PrConfiguracionesElementosVO1");
        Row row = view.createRow();

        row.setAttribute("PrTmprSerie", 2100);
        row.setAttribute("PrTmprIdentificador", 1);
        row.setAttribute("PrElemSerie", 2100);
        row.setAttribute("PrElemIdentificador", 1);
        row.setAttribute("Orden", 12345);
        row.setAttribute("Modificable", "S");

        am.getDBTransaction().commit();

        row.setAttribute("PrTmprSerie", 2100);
        row.setAttribute("PrTmprIdentificador", 2);
        row.setAttribute("PrElemSerie", 2100);
        row.setAttribute("PrElemIdentificador", 2);
        am.getDBTransaction().commit();

        row.remove();
        am.getDBTransaction().commit();

    }

    /**
     * Crea los registros que se necesitan para el m�todo testInsertUpdateDelete
     * de esta prueba. Este metodo con la anotacion '@Before' se ejecuta antes
     * de todos los m�todos contenidos en esta clase de prueba.
     *
     * @author Jorge Hern�ndez
     * @since 12-SEP-2011
     */
    @BeforeClass
    public static void setUp() {
        ApplicationModule applicationModule = fixture1.getApplicationModule();
        CatalogoSencilloAMImpl am = (CatalogoSencilloAMImpl)applicationModule;
        DBTransaction bTransaction = am.getDBTransaction();
        Statement stmt = bTransaction.createStatement(1);
        try {
            stmt.executeUpdate("Insert into PR_TIPOS_MOVIMIENTOS_PREDIOS (SERIE,IDENTIFICADOR,CLAVE,NOMBRE,DESCRIPCION,MODULO,ESTATUS,GENERACOBRO,PROCESOCOBRO,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5,CAMPO6,CAMPO7,CAMPO8,CAMPO9,CAMPO10,CAMPO11,CAMPO12,CAMPO13,CAMPO14,CAMPO15,CREADO_POR,CREADO_EL,MODIFICADO_POR,MODIFICADO_EL) values (2100,1,'TEST1','PRUEBA1','PRUEBA1','MODULO','AC','S',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,'anonymous',to_timestamp('15-SEP-99','DD-MON-RR HH.MI.SSXFF AM'),'anonymus',to_timestamp('15-SEP-99','DD-MON-RR HH.MI.SSXFF AM'))");
            stmt.executeUpdate("Insert into PR_TIPOS_MOVIMIENTOS_PREDIOS (SERIE,IDENTIFICADOR,CLAVE,NOMBRE,DESCRIPCION,MODULO,ESTATUS,GENERACOBRO,PROCESOCOBRO,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5,CAMPO6,CAMPO7,CAMPO8,CAMPO9,CAMPO10,CAMPO11,CAMPO12,CAMPO13,CAMPO14,CAMPO15,CREADO_POR,CREADO_EL,MODIFICADO_POR,MODIFICADO_EL) values (2100,2,'TEST2','PRUEBA2','PRUEBA2','MODULO','AC','S',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,'anonymous',to_timestamp('15-SEP-99','DD-MON-RR HH.MI.SSXFF AM'),'anonymus',to_timestamp('15-SEP-99','DD-MON-RR HH.MI.SSXFF AM'))");
            stmt.executeUpdate("Insert into PR_ELEMENTOS (SERIE,IDENTIFICADOR,CLAVE,NOMBRE,DESCRIPCION,DOMINIO,ETIQUETA,COMPORTAMIENTO,FORMULA_VALIDACION,NIVEL,PR_ELEM_SERIE,PR_ELEM_IDENTIFICADOR,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5,CAMPO6,CAMPO7,CAMPO8,CAMPO9,CAMPO10,CAMPO11,CAMPO12,CAMPO13,CAMPO14,CAMPO15,CREADO_POR,CREADO_EL,MODIFICADO_POR,MODIFICADO_EL) values (2100,1,'01','Elemento','Primer elemento',null,'Primer elemento',null,null,1,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,'anonymus',to_timestamp('15-SEP-99','DD-MON-RR HH.MI.SSXFF AM'),'anonymus',to_timestamp('15-SEP-99','DD-MON-RR HH.MI.SSXFF AM'))");
            stmt.executeUpdate("Insert into PR_ELEMENTOS (SERIE,IDENTIFICADOR,CLAVE,NOMBRE,DESCRIPCION,DOMINIO,ETIQUETA,COMPORTAMIENTO,FORMULA_VALIDACION,NIVEL,PR_ELEM_SERIE,PR_ELEM_IDENTIFICADOR,CAMPO1,CAMPO2,CAMPO3,CAMPO4,CAMPO5,CAMPO6,CAMPO7,CAMPO8,CAMPO9,CAMPO10,CAMPO11,CAMPO12,CAMPO13,CAMPO14,CAMPO15,CREADO_POR,CREADO_EL,MODIFICADO_POR,MODIFICADO_EL) values (2100,2,'02','Elemento','Segundo elemento',null,'Segundo elemento',null,null,1,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,'anonymus',to_timestamp('15-SEP-99','DD-MON-RR HH.MI.SSXFF AM'),'anonymus',to_timestamp('15-SEP-99','DD-MON-RR HH.MI.SSXFF AM'))");
        } catch (SQLException ex) {
            ex.printStackTrace();
            bTransaction.rollback();
        }
        bTransaction.commit();
    }

    /**
     * Elimina los registros que se prepararon en el metodo setUp() de esta
     * prueba. Este metodo con la anotacion '@After' se ejecuta despues de
     * todos los m�todos contenidos en esta clase de prueba.
     *
     * @author Jorge Hern�ndez
     * @since 12-SEP-2011
     */
    @AfterClass
    public static void tearDown() {
        ApplicationModule applicationModule = fixture1.getApplicationModule();
        CatalogoSencilloAMImpl am = (CatalogoSencilloAMImpl)applicationModule;
        DBTransaction bTransaction = am.getDBTransaction();
        Statement stmt = bTransaction.createStatement(1);
        try {
            stmt.executeUpdate("Delete from PR_TIPOS_MOVIMIENTOS_PREDIOS where serie = 2100 and identificador in  (1,2)");
            stmt.executeUpdate("Delete from PR_ELEMENTOS where serie = 2100 and identificador in  (1,2)");
        } catch (SQLException ex) {
            ex.printStackTrace();
            bTransaction.rollback();
        }
        bTransaction.commit();
    }
}
