package mx.tgc.model.module.view.PrTiposMovimientosPredios1VO;

import mx.tgc.model.module.CatalogoSencilloAMImpl;
import mx.tgc.model.module.applicationModule.CatalogoSencilloAMFixture;

import oracle.jbo.ApplicationModule;
import oracle.jbo.Row;
import oracle.jbo.ViewObject;

import org.junit.*;
import static org.junit.Assert.*;

public class PrTiposMovimientosPredios1VOTest {
    private CatalogoSencilloAMFixture fixture1 =
        CatalogoSencilloAMFixture.getInstance();

    public PrTiposMovimientosPredios1VOTest() {
    }

    /**
     * M�todo que sirve para probar la creaci�n de la instancia de la vista.
     *
     * @author Isaac S�as Guti�rrez
     * @since 19-SEP-2011
     */
    @Test
    public void testCreacionInstancia() {
        ApplicationModule am = fixture1.getApplicationModule();
        ViewObject view = am.findViewObject("PrTiposMovimientosPrediosVO1");
        assertNotNull(view);
    }

    /**
     * M�todo que sirve para probar los campos obligatorios
     * verifica que efectivamente si no se llenan los campos obligatorios
     * al validar el nuevo registro debe arrojar error.
     *
     * @author Isaac S�as Guti�rrez
     * @since 19-SEP-2011
     */
    @Test
    public void testCamposObligatorios() {
        CatalogoSencilloAMImpl am =
            (CatalogoSencilloAMImpl)fixture1.getApplicationModule();
        ViewObject view = am.findViewObject("PrTiposMovimientosPrediosVO1");
        Row row = view.createRow();

        //TODO: por cada atributo obligatorio en la vista implementar lo siguiente:
        //NOTA: los campos creado_el, creado_por, etc. si se configuraron adecuadamente en la
        //      entidad para llenarse de forma autom�tica no es necesario llenarlos
        //      en este m�todo.
        row.setAttribute("Serie", 2011);
        row.setAttribute("Identificador", 999999999999999L);
        row.setAttribute("Clave", "CLAVE");
        row.setAttribute("Nombre", "NOMBRE");
        row.setAttribute("Modulo", "MODULO");
        row.setAttribute("Estatus", "AC");
        row.setAttribute("Generacobro", "S");
        //-------------
        row.validate();
        am.getDBTransaction().rollback();
    }

    /**
     * M�todo utilizado para probar la longitud de los campos
     * de una vista de datos, el m�todo prueba que las longitudes
     * sean probadas con la m�xima longitud permitida para los
     * campos de tipo String (VARCHAR2).
     *
     * @author Isaac S�as Guti�rrez
     * @since 19-SEP-2011
     */
    @Test
    public void testPresicionMaximaEnCampos() {
        CatalogoSencilloAMImpl am =
            (CatalogoSencilloAMImpl)fixture1.getApplicationModule();
        ViewObject view = am.findViewObject("PrTiposMovimientosPrediosVO1");
        Row row = view.createRow();


        //TODO: variables predefinidas con varias longitudes para
        //probar el m�ximo de caracteres permitidos en los campos de las tablas
        String campo2000Chars = "";
        for (int i = 0; i < 2000; i++) {
            campo2000Chars += "1";
        }

        String campo500Chars = "";
        for (int i = 0; i < 500; i++) {
            campo500Chars += "1";
        }

        String campo120Chars = "";
        for (int i = 0; i < 120; i++) {
            campo120Chars += "1";
        }

        String campo100Chars = "";
        for (int i = 0; i < 100; i++) {
            campo100Chars += "1";
        }

        String campo30Chars = "";
        for (int i = 0; i < 30; i++) {
            campo30Chars += "1";
        }

        String campo20Chars = "";
        for (int i = 0; i < 20; i++) {
            campo20Chars += "1";
        }

        String campo15Chars = "";
        for (int i = 0; i < 15; i++) {
            campo15Chars += "1";
        }

        String campo4Chars = "";
        for (int i = 0; i < 4; i++) {
            campo4Chars += "1";
        }

        String campo2Chars = "";
        for (int i = 0; i < 2; i++) {
            campo2Chars += "1";
        }

        String campo1Chars = "";
        for (int i = 0; i < 1; i++) {
            campo1Chars += "1";
        }

        //TODO: por cada atributo en la vista implementar lo siguiente:
        //NOTA: los campos creado_el, creado_por, etc. si se configuraron adecuadamente en la
        //      entidad para llenarse de forma autom�tica no es necesario llenarlos
        //      en este m�todo.
        row.setAttribute("Serie", Integer.parseInt(campo4Chars));
        row.setAttribute("Identificador", Long.parseLong(campo15Chars));
        row.setAttribute("Clave", campo20Chars);
        row.setAttribute("Nombre", campo100Chars);
        row.setAttribute("Descripcion", campo500Chars);
        row.setAttribute("Modulo", campo120Chars);
        row.setAttribute("Estatus", campo2Chars);
        row.setAttribute("Generacobro", campo1Chars);
        row.setAttribute("Procesocobro", campo100Chars);
        row.setAttribute("Campo1", campo2000Chars);
        row.setAttribute("Campo2", campo2000Chars);
        row.setAttribute("Campo3", campo2000Chars);
        row.setAttribute("Campo4", campo2000Chars);
        row.setAttribute("Campo5", campo2000Chars);
        row.setAttribute("Campo6", campo2000Chars);
        row.setAttribute("Campo7", campo2000Chars);
        row.setAttribute("Campo8", campo2000Chars);
        row.setAttribute("Campo9", campo2000Chars);
        row.setAttribute("Campo10", campo2000Chars);
        row.setAttribute("Campo11", campo2000Chars);
        row.setAttribute("Campo12", campo2000Chars);
        row.setAttribute("Campo13", campo2000Chars);
        row.setAttribute("Campo14", campo2000Chars);
        row.setAttribute("Campo15", campo2000Chars);
        //-------------------
        row.validate();
        am.getDBTransaction().rollback();
    }

    /**
     * M�todo utilizado para probar altas y
     * bajas de registros en la vista de datos
     * se crea un nuevo row al cual se le llena con datos
     * de prueba para poder probar las operaciones de
     * insert y delete.
     *
     * @author Isaac S�as Guti�rrez
     * @since 19-SEP-2011
     */
    @Test
    public void testInsertUpdateDelete() {
        CatalogoSencilloAMImpl am =
            (CatalogoSencilloAMImpl)fixture1.getApplicationModule();
        ViewObject view = am.findViewObject("PrTiposMovimientosPrediosVO1");
        Row row = view.createRow();

        //TODO: Llenar cada atributo de la vista con valores para probar los inserts y deletes
        //NOTA: los campos creado_el, creado_por, etc. si se configuraron adecuadamente en la
        //      entidad para llenarse de forma autom�tica no es necesario llenarlos
        //      en este m�todo.
        row.setAttribute("Serie", 2011);
        row.setAttribute("Identificador", 999999999999999L);
        row.setAttribute("Clave", "CLAVE");
        row.setAttribute("Nombre", "NOMBRE");
        row.setAttribute("Modulo", "MODULO");
        row.setAttribute("Estatus", "AC");
        row.setAttribute("Generacobro", "S");
        am.getDBTransaction().commit();
        //-------------------
        row.setAttribute("Generacobro", "N");
        am.getDBTransaction().commit();
        //-------------------
        row.remove();
        am.getDBTransaction().commit();
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }
}
